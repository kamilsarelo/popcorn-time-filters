// use F12 (Windows) or CMD+0 (macOS) to open the JavaScript console in Popcorn Time

// make sure this is alawys a singleton ////////////////////////////////////////

if (timerFilter) {
	clearInterval(timerFilter);
}
if (timerComponent) {
	clearInterval(timerComponent);
}

// setup filter timer //////////////////////////////////////////////////////////

var yearMin = 0;
var ratingMin = 0;
var timerFilter = setInterval(function() {
	var nodeList = document.querySelectorAll("#content > div > div.list-region > ul > div.items > li");
	nodeList.forEach(function(node) {
		if (node != null) { // equivalent to: typeof(node) !== "undefined" && node !== null
			var year = node.querySelector("p.year");
			var rating = node.querySelector("div.rating-value");
			if (year && parseInt(year.innerText.trim()) >= yearMin
				&& rating && parseInt(rating.innerText[0]) >= ratingMin
			) {
				node.style.display = 'block';
				return;
			}
			node.style.display = 'none';
		}
	});
}, 1000);

// create and append filter UI elements ////////////////////////////////////////

var timerComponent = setInterval(function() { // required because filter bar is re-created on each section switch

	// check if already exits //////////////////////////////////////////////////

	var parent = document.querySelector('#nav-filters');
	if (!parent) {
		throw new Error('Cannot find filters bar in document');
	}
	var component = parent.querySelector('#com_kamilsarelo_component');
	if (component) {
		return;
	}

	// create and append ///////////////////////////////////////////////////////

	component = document.createElement('li');
	component.className = 'dropdown filter';
	component.id = 'com_kamilsarelo_component';
	component.style = 'padding: 5px; background-color: #FDFD95; font-weight: bold;';
	component.innerHTML = 'Filter by min year <select id="com_kamilsarelo_select_year"></select> min rating <select id="com_kamilsarelo_select_rating"></select>';

	parent.appendChild(component); // https://stackoverflow.com/questions/17627587/how-to-access-element-created-by-javascript-document-createelement-function

	// prefill the filters /////////////////////////////////////////////////////

	var selectYear = document.getElementById('com_kamilsarelo_select_year');
	{
		var optionYear = document.createElement('option');
		optionYear.value = 0;
		optionYear.text = 'all';
		selectYear.appendChild(optionYear);
	}
	for (var year = new Date().getFullYear(); year >= 1950; year--) {
		var optionYear = document.createElement('option');
		optionYear.value = year;
		optionYear.text = year;
		selectYear.appendChild(optionYear);
	}

	var selectRating = document.getElementById('com_kamilsarelo_select_rating');
	for (var rating = 0; rating <= 9; rating++) {
		var optionRating = document.createElement('option');
		optionRating.value = rating;
		optionRating.text = rating;
		selectRating.appendChild(optionRating);
	}

	// attach event handlers to filters ////////////////////////////////////////

	selectYear.onchange = function() {
		yearMin = this.value;
	};
	selectRating.onchange = function() {
		ratingMin = this.value;
	};

}, 1000);
